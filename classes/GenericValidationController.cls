/**
* @author Dev Khatri
* @date 1/4/2020
*
*
* @description This is controller for GenericValidation.cmp
*/

public class GenericValidationController {
    
    @AuraEnabled
    public static Map<String,String> isRecordExists(String targetObject, String targetField, String value){
       Map<String,String> response = new Map<String,String>();
        response.put('isExist','False');
        try{
            String query = 'Select count() from ' + targetObject + ' where ' + targetField + ' = ' + '\'' + value + '\'';
            Integer recordsCount = Database.countQuery(query);
            if(recordsCount != 0){
                response.put('isExist','True');
            }
        }catch(Exception e){
            response.put('ApexError',e.getMessage());
        }
        return response;
    } 
}