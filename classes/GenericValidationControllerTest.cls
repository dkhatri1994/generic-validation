/**
* @author Dev Khatri
* @date 1/4/2020
*
*
* @description This is test class of GenericValidationController
*/

@isTest
public class GenericValidationControllerTest {

     @testSetup static void setup() {
        // Create common test contact
            Contact contact=new Contact(
            FirstName='fname',
            LastName = 'lname',
            Email = 'test@email.com',
            Phone = '971212121'); 
        insert contact; 
    }
    
    @isTest static void testisRecordExists(){
       Map<String,String> response = GenericValidationController.isRecordExists('Contact', 'Email', 'test@email.com');
       system.assertEquals('True', response.get('isExist')); 
    }
    
    @isTest static void testisRecordExistsNegative(){
       Map<String,String> response = GenericValidationController.isRecordExists('Contact__c', 'Email', 'test@email.com');
       system.assertEquals('False', response.get('isExist')); 
    }
    
    
}