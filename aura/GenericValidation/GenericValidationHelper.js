({    
    // This array will store framework related errors
    frameworkErrorList: [],
    
    // This method will reset validation flag and framework errors.
    resetValidationFlag : function(component, helper) {
        component.set("v.isValidationSuccess", true);
        helper.frameworkErrorList = [];   
    },
    
    // This method will use to get final status of validation.
    getValidationStatus : function(component, helper){
        var finalStatus = component.get("v.isValidationSuccess");
        helper.resetValidationFlag(component, helper);  
        return finalStatus;
    },
    
    // This method will be use to check valid age.
    checkValidAge : function(component, helper, auraId, validationType, age, errorMessage){
        
        return new Promise(function (resolve, reject){
            try{
                var inputType = component.find(auraId);
                var value = inputType.get("v.value");
                
                // Here we are checking is user inserted value or not.
                if(value === "" || value === null){
                    helper.setValidationMessage(component, helper, inputType, "Please fill this!!");
                    return resolve("Success");         
                }
                
                var currentDate = new Date();
                var yearDifferance = helper.yearBetweenTwoDates(helper.getCurrentDate(), value);
                
                if(helper.compare(yearDifferance, validationType, age)){
                    helper.removeValidationMessage(inputType);
                }else{
                    helper.setValidationMessage(component, helper, inputType, errorMessage);
                    
                } 
                resolve("Success");
            }catch (exception){
                helper.setValidationMessage(component, helper, null, exception);
                reject(exception);
            }
        });
        
        
    },
    
    //This method will be use to check the valid length.
    checkValidLength : function(component, helper, auraId, validationType, length, errorMessage){
        
        return new Promise(function (resolve, reject){
            try{
                var inputType = component.find(auraId);
                var value = inputType.get("v.value");
                
                // Here we are checking is user inserted value or not.
                if(value === "" || value === null){
                    helper.setValidationMessage(component, helper, inputType, "Please fill this!!");
                    return resolve("Success");         
                }
                
                if(helper.compare(value.toString().length, validationType, length)){
                    helper.removeValidationMessage(inputType);
                }else{
                    helper.setValidationMessage(component, helper, inputType, errorMessage);                
                }
                resolve("Success");
            }catch (exception){
                helper.setValidationMessage(component, helper, null, exception);
                reject(exception);
            }
        });    
    },
    
    //This method use to check is record already exists in salesforce.
    checkRecordExists : function(component, helper, auraId, targetObject, targetField, message){
        return new Promise(function(resolve,reject){

                var inputType = component.find(auraId);
                var value = inputType.get("v.value");
                
                // Here we are checking is user inserted value or not.
                if(value == ""){
                    helper.setValidationMessage(component, helper, inputType, "Please fill this!!");
                    return resolve("Success");         
                }

                var isRecordExist = false;
                
                var action = component.get("c.isRecordExists");
                action.setParams({
                    "targetObject": targetObject,
                    "targetField": targetField,
                    "value": value     
                });
                
                action.setCallback(this, function(response){
                    var state = response.getState();                        
                    if(state == "SUCCESS"){
                        if(response.getReturnValue().isExist === "False" && response.getReturnValue().ApexError){
                            helper.setValidationMessage(component, helper, null, response.getReturnValue().ApexError);
                            return reject(response.getReturnValue().ApexError);
                        }else if(response.getReturnValue().isExist === "True"){
                            helper.setValidationMessage(component, helper, inputType, message);            
                        }else{                        
                            helper.removeValidationMessage(inputType);
                        }
                        resolve("Sucess");                        
                    }else{
                        reject("Error");
                    }
                
                });  
                $A.enqueueAction(action);    
        }); 
    },
    
    // This method will use to set error message with validation flag.
    setValidationMessage : function(component, helper, inputType, message){
        component.set("v.isValidationSuccess", false);     
        if(inputType != undefined || inputType != null){
            inputType.setCustomValidity(message);
            inputType.reportValidity();
        }else{
            helper.frameworkErrorList.push(message); 
        }
       
    },
    
    //This is used to show show toast messages.
    showToastMessage : function(component, helper, event, title, customMessage, toastType) {
        var toastEvent = $A.get("e.force:showToast");
        toastEvent.setParams({
            title : title,
            message: customMessage,
            type: toastType
        });
        toastEvent.fire();
    },
    
    // This method will be use to remove already added error message.
    removeValidationMessage : function(inputType){
        inputType.setCustomValidity("");
        inputType.reportValidity();
    },
    
    // This method will return years between two dates.
    yearBetweenTwoDates : function(newDate, oldDate){
        var calculatedYear = 0;
        var newDateYear = newDate.split("-")[0];
        var newDateMonth = newDate.split("-")[1];
        var newDateDay = newDate.split("-")[2]; 
        
        var oldDateYear = oldDate.split("-")[0];
        var oldDateMonth = oldDate.split("-")[1];
        var oldDateDay = oldDate.split("-")[2];
        
        var calculatedYear = newDateYear - oldDateYear;
        
        if (newDateMonth < oldDateMonth - 1) {
            calculatedYear--;
        }
        if (oldDateMonth - 1 == newDateMonth && newDateDay < oldDateDay) {
            calculatedAge--;
        }
        return calculatedYear;
    },
    
    // This method can be use to get current date with YYYY-MM-DD format.
    getCurrentDate : function(){
        
        var date = new Date(),
            month = "" + (date.getMonth() + 1),
            day = "" + date.getDate(),
            year = date.getFullYear();
        
        if (month.length < 2) 
            month = "0" + month;
        if (day.length < 2) 
            day = "0" + day;
        
        return [year, month, day].join("-");
    },
    
    // This method will compare two values as per given operators and return result as boolean.
    compare : function(post, operator, value) {
        switch (operator) {
            case ">":   return post > value;
            case "<":   return post < value;
            case ">=":  return post >= value;
            case "<=":  return post <= value;
            case "==":  return post == value;
            case "!=":  return post != value;
        }
    }
    
    
})