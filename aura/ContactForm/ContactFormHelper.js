({
    checkValidation : function(component, helper) {
        Promise.all([
            helper.checkValidAge(component, helper, "birthDate", ">=", 18, "Age should be 18 or more"),
            helper.checkRecordExists(component, helper, "email", "Contact", "Email", "Email already exists!!!"),
            helper.checkValidLength(component, helper, "name", "<=", 40, "Name length should be 40 or less!!!"),
            
        ]).then(
            function(response) {
                var finalValidationStatus = helper.getValidationStatus(component, helper);
                if(finalValidationStatus){
                    helper.showToastMessage(component, helper, event, "Success", "Validation successful", "success");
                    // or Business logic here!!!!!
                }else{
                   helper.showToastMessage(component, helper, event, "Failed", "Validation unsuccessful", "error"); 
                }
            }
        ).catch(
            function(errorMsg) {
				helper.showToastMessage(component, helper, event, "Failed", errorMsg, "error"); 
            }
        );
        
    }
})